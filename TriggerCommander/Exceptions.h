/*
 * Exceptions.h
 *
 *  Created on: Jun 25, 2013
 *      Author: avolio
 */

#ifndef DAQ_TRIGGER_COMMANDER_EXCEPTIONS_H_
#define DAQ_TRIGGER_COMMANDER_EXCEPTIONS_H_

#include <ers/ers.h>

namespace daq {
    ERS_DECLARE_ISSUE(trigger, Exception, ERS_EMPTY, ERS_EMPTY)

    ERS_DECLARE_ISSUE_BASE
    (trigger,
     CommandFailed,
     trigger::Exception,
     "An error occurred during command: " << command << ". Reason: " << reason,
     ERS_EMPTY,
     ((const char *)command)
     ((const char *)reason)
    )

    ERS_DECLARE_ISSUE_BASE
    (trigger,
     CorbaException,
     trigger::Exception,
     "Received exception " << excp << " when interacting with " << src,
     ERS_EMPTY,
     ((const char*) excp)
     ((const char*) src)
    )

    ERS_DECLARE_ISSUE_BASE
    (trigger,
     IPCLookup,
     trigger::Exception,
     "Failed to lookup " << server << " in IPC.",
     ERS_EMPTY,
     ((const char*) server)
    )

    ERS_DECLARE_ISSUE_BASE
    (trigger,
     InvalidCommand,
     trigger::Exception,
     "Command '" << cmd << "' with args '" << args << "' cannot be processed: " << reason,
     ERS_EMPTY,
     ((const char *)cmd )
     ((const char *)args )
     ((const char *)reason )
    )

    ERS_DECLARE_ISSUE_BASE
    (trigger,
     TriggerNotResumed,
     trigger::Exception,
     "The master trigger denied to resume the trigger: " << reason,
     ERS_EMPTY,
     ((const char*) reason)
    )

    ERS_DECLARE_ISSUE_BASE
    (trigger,
     CmdlParsing,
     trigger::Exception,
     "Parsing of command line parameters failed: " << reason,
     ERS_EMPTY,
     ((const char*) reason)
    )
}

#endif /* DAQ_TRIGGER_COMMANDER_EXCEPTIONS_H_ */
