#ifndef DAQ_TRIGGER_MASTER_TRIGGER_H_
#define DAQ_TRIGGER_MASTER_TRIGGER_H_

#include "TriggerCommander/HoldTriggerInfo.h"

#include <sys/types.h>

namespace daq { namespace trigger {
/**
 * @page TrigComMain Overview of the TriggerCommander Package
 *
 * @tableofcontents
 *
 * All the data structures in this package are defined in the @em daq::trigger namespace.
 *
 * @section MTOverview Overview
 * The TriggerCommander package contains the daq::trigger::MasterTrigger interface to be
 * implemented by all the applications that have the rule of Master Trigger in a partition.
 *
 * Since the Master Trigger has to be remotely accessed, it needs to be available via the standard
 * IPC communication mechanism. In order to do that, an instance of the daq::trigger::CommandedTrigger
 * class has to be instantiated: it exposes a remote interface acting as a proxy to the underlining
 * daq::trigger::MasterTrigger interface.
 *
 * The Master Trigger is usually a Run Control application, that means the corresponding application
 * has to implement the daq::rc::Controllable interface as well (see the RunControl package
 * documentation for additional details).
 *
 * This package contains also two utility classes:
 * @li daq::trigger::TriggerCommander: to be used to send commands to the Master Trigger;
 * @li daq::trigger::AutoPrescalerCommander: to be used to send commands to the Trigger AutoPrescaler.
 *
 * Applications using the Master Trigger functionality should link against the <em>libtrgCommander.so</em>
 * shared library.
 *
 * @section MTLBInfo Publication of Information About Luminosity Blocks
 * Information about the LB shall be published in two IS servers: @em RunParams.LumiBlock and @em RunParams.LBSettings.
 * @li @em RunParams.LumiBlock shall contain the current LB, Run number and the time stamp of the LB change. The IS information
 * structure is described in @em TTCInfo/LumiBlock.h and @em TTCInfo/LumiBlockNamed.h, where the second
 * header file is the “named” version of the IS structure (data structures are LumiBlock and LumiBlockNamed).
 * @li @em RunParams.LBSettings shall contain information about the way the LB is increased (regularly in time
 * – “TIME” – or based on the number of recorded events - “EVENTS”), the value of the threshold
 * causing a LB change and the minimum length of an LB interval. The IS information structure is described
 * in @em TTCInfo/LBSettings.h and @em TTCInfo/LBSettingsNamed.h (data structures are LBSettings and LBSettingsNamed).
 *
 * @section MTExa Examples
 * <a href="https://gitlab.cern.ch/atlas-tdaq-software/TriggerCommander/-/blob/master/examples/ctp_emulator.cxx">This link</a>
 * contains a real example of how A Run Control application may implement the MasterTrigger interface. Note that:
 * @li The main function looks exactly as the one for a standard Run Control application;
 * @li The MasterTrigger instance is created and destroyed during the <em>configure</em> and <em>unconfigure</em> transitions.
 */

/**
 * @class daq::trigger::MasterTrigger
 *
 * This is the interface for the Master Trigger.
 * <p>
 * It is a Master Trigger task to hold/resume the trigger, change the current trigger pre-scale sets
 * and manager the Luminosity Blocks.
 * <p>
 * The failure in executing any of the defined action has to be notified throwing any exception inheriting from ers::Issue.
 * For the daq::trigger::MasterTrigger::resume() method the <em>daq::trigger::TriggerNotResumed</em> exception can
 * be used in order to notify that the trigger could not be resumed for a known (under control) reason.
 */
class MasterTrigger {
    public:
        /**
         * Destructor
         */
        virtual ~MasterTrigger() {}

        /**
         * It puts the trigger on hold
         *
         * @param dm String-encoded bit mask identifying the detectors causing the trigger to be held;
         *           an empty string means that the trigger-on-hold is not related to any specific detector
         *
         * @return A data structure holding information about the the current ECR counter and the
         *         last @em valid extended L1 ID
         */
        virtual HoldTriggerInfo hold(const std::string& dm) = 0;

        /**
         * It resumes the trigger
         *
         * @param dm String-encoded bit mask identifying the detectors causing the trigger to be resumed;
         *           an empty string means that the resume-trigger is not related to any specific detector
         */
        virtual void resume(const std::string& dm) = 0;

        /**
         * It sets the trigger pre-scales
         *
         * @param l1p The L1 pre-scale key
         * @param hltp The HLT pre-scale key
         */
        virtual void setPrescales(uint32_t l1p, uint32_t hltp) = 0;

        /**
         * It sets the L1 trigger pre-scales
         *
         * @param l1p The L1 pre-scale key
         */
        virtual void setL1Prescales(uint32_t l1p) = 0;

        /**
         * It sets the HLT trigger pre-scales
         *
         * @param hltp The HLT pre-scale key
         */
        virtual void setHLTPrescales(uint32_t hltp) = 0;

        /**
         * It increases the Luminosity Block
         *
         * @param runno The run number
         */
        virtual void increaseLumiBlock(uint32_t runno) = 0;

        /**
         * It sets the time interval between two Luminosity Block
         *
         * @param seconds The time interval (in seconds) between to Luminosity Blocks
         */
        virtual void setLumiBlockInterval(uint32_t seconds) = 0;

        /**
         * It sets the minimum length of a Luminosity Block
         *
         * @param seconds The minimum length (in seconds) of a Luminosity Block
         */
        virtual void setMinLumiBlockLength(uint32_t seconds) = 0;

        /**
         * It sets the new Bunch Group
         *
         * @param bg The new Bunch Group
         */
        virtual void setBunchGroup(uint32_t bg) = 0;

        /**
         * It sets the update of some conditions data
         *
         * @param folderIndex The folder index for the new conditions
         * @param lb The Luminosity Block number
         */
        virtual void setConditionsUpdate(uint32_t folderIndex, uint32_t lb) = 0;

        /**
         * It changes the L1 and HLT prescale and the bunch group keys
         *
         * @param l1p The L1 prescale key
         * @param hltp The HLT prescale key
         * @param bg The bunch group key
         */
        virtual void setPrescalesAndBunchgroup(uint32_t l1p, uint32_t hltp, uint32_t bg) = 0;
};

}}

#endif
