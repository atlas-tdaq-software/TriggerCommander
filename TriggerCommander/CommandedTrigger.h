#ifndef DAQ_TRIGGER_COMMANDEDTRIGGER_H_
#define DAQ_TRIGGER_COMMANDEDTRIGGER_H_

#include <string_view>

#include <ipc/object.h>

#include "trgCommander/trgCommander.hh"

class IPCPartition;

namespace daq { namespace trigger {

class MasterTrigger;

/**
 * This class acts as a wrapper for the MasterTrigger interface, allowing an object
 * implementing that interface to be published in IPC and then able to receive remote calls.
 * <p>
 * Before calling the underlining MasterTrigger methods, this class checks with the Access
 * Manager system whether the user is allowed to execute a defined command.
 * <p>
 * An object of this class must always be created on the heap and destroyed using the <em>_destroy()</em>
 * method.
 * <p>
 * If the environment variable TDAQ_MT_VERBOSE is set to any value, then this class will log any
 * received command, including the name of the user sending the command and the origin host.
 */
class CommandedTrigger: public IPCNamedObject<POA_TRIGGER::commander, ::ipc::multi_thread> {
    public:
        /**
         * Constructor.
         *
         * It case care of publishing its reference in IPC, so that it can be reached remotely.
         *
         * \param p The IPC partition
         * \param name The name to be used for the IPC publication
         * \param mt A pointer to the object implementing the MasterTrigger interface
         */
        explicit CommandedTrigger(const IPCPartition& p, const std::string& name, MasterTrigger* const mt);

        /**
         * Destructor.
         */
        virtual ~CommandedTrigger();

        /**
         * It takes care of removing the reference from IPC and destroys this object
         * After a call of this method the object should not be used anymore
         */
        virtual void shutdown() override;

        /**
         * It calls daq::trigger::MasterTrigger::hold on the MasterTrigger.
         * This method can be called remotely via CORBA.
         *
         * \param sc The sender's context
         * \param dm String-encoded bit mask identifying the detectors causing the trigger to be held;
         *           an empty string means that the trigger-on-hold is not related to any specific detector
         * \param triggerInfo Output parameter holding information about the current ECR value and the last
         *        valid extended L1 ID.
         * \throw TRIGGER::CommandExecutionFailed
         */
         void hold(const TRIGGER::SenderContext& sc, const char* dm, TRIGGER::HoldTriggerInfo& triggerInfo) override;


        /**
         * It calls daq::trigger::MasterTrigger::resume on the MasterTrigger
         * This method can be called remotely via CORBA.
         *
         * \param sc The sender's context
         * \param dm String-encoded bit mask identifying the detectors causing the trigger to be resumed;
         *           an empty string means that the resume-trigger is not related to any specific detector
         * \throw TRIGGER::CommandExecutionFailed
         * \throw TRIGGER::TriggerNotResumed
         */
        void resume(const TRIGGER::SenderContext& sc, const char* dm) override;

        /**
         * It calls daq::trigger::MasterTrigger::setPrescales on the MasterTrigger
         * This method can be called remotely via CORBA.
         *
         * \param sc The sender's context
         * \param l1p The L1 prescale key
         * \param hltp The HLS prescale key
         * \throw TRIGGER::CommandExecutionFailed
         */
        void setPrescales(const TRIGGER::SenderContext& sc, ::CORBA::ULong l1p, ::CORBA::ULong hltp) override;

        /**
         * It calls daq::trigger::MasterTrigger::setL1Prescales on the MasterTrigger
         * This method can be called remotely via CORBA.
         *
         * \param sc The sender's context
         * \param l1p The L1 prescale key
         * \throw TRIGGER::CommandExecutionFailed
         */
        void setL1Prescales(const TRIGGER::SenderContext& sc, ::CORBA::ULong l1p) override;

        /**
         * It calls daq::trigger::MasterTrigger::setHLTPrescales on the MasterTrigger
         * This method can be called remotely via CORBA.
         *
         * \param sc The sender's context
         * \param hltp The HLS prescale key
         * \throw TRIGGER::CommandExecutionFailed
         */
        void setHLTPrescales(const TRIGGER::SenderContext& sc, ::CORBA::ULong hltp) override;

        /**
         * It calls daq::trigger::MasterTrigger::increaseLumiBlock on the MasterTrigger
         * This method can be called remotely via CORBA.
         *
         * \param sc The sender's context
         * \param runno The run number
         * \throw TRIGGER::CommandExecutionFailed
         */
        void increaseLumiBlock(const TRIGGER::SenderContext& sc, ::CORBA::ULong runno) override;

        /**
         * It calls daq::trigger::MasterTrigger::setLumiBlockInterval on the MasterTrigger
         * This method can be called remotely via CORBA.
         *
         * \param sc The sender's context
         * \param seconds The new value of the Luminosity block interval
         * \throw TRIGGER::CommandExecutionFailed
         */
        void setLumiBlockInterval(const TRIGGER::SenderContext& sc, ::CORBA::ULong seconds) override;

        /**
         * It calls daq::trigger::MasterTrigger::setMinLumiBlockLength on the MasterTrigger
         * This method can be called remotely via CORBA.
         *
         * \param sc The sender's context
         * \param seconds The new value of the minimum length of Luminosity block interval
         * \throw TRIGGER::CommandExecutionFailed
         */
        void setMinLumiBlockLength(const TRIGGER::SenderContext& sc, ::CORBA::ULong seconds) override;

        /**
         * It calls daq::trigger::MasterTrigger::setBunchGroup on the MasterTrigger
         * This method can be called remotely via CORBA.
         *
         * \param sc The sender's context
         * \param bg The value of the new BG
         * \throw TRIGGER::CommandExecutionFailed
         */
        void setBunchGroup(const TRIGGER::SenderContext& sc, ::CORBA::ULong bg) override;

        /**
         * It calls daq::trigger::MasterTrigger::setConditionsUpdate on the MasterTrigger
         * This method can be called remotely via CORBA.
         *
         * \param sc The sender's context
         * \param folderIndex The index of the folder for the new conditions
         * \param lb The value of the current LB
         * \throw TRIGGER::CommandExecutionFailed
         */
        void setConditionsUpdate(const TRIGGER::SenderContext& sc, ::CORBA::ULong folderIndex, ::CORBA::ULong lb) override;

        /**
         * It calls daq::trigger::MasterTrigger::setPrescalesAndBunchgroup on the MasterTrigger
         * This method can be called remotely via CORBA.
         *
         * \param sc The sender's context
         * \param l1p The L1 prescale key
         * \param hltp The HLT prescale key
         * \param bg The bunch group key
         * \throw TRIGGER::CommandExecutionFailed
         */
        void setPrescalesAndBunchgroup(const TRIGGER::SenderContext& sc, ::CORBA::ULong l1p, ::CORBA::ULong hltp, ::CORBA::ULong bg) override;

    private:
        void allowedByAM(const char * userName, const std::string& command, const std::string& args = "");
        std::string verifyUserCredentials(std::string_view userCredentials) const;

        MasterTrigger* const mt_;
        std::string m_processOwner;
        const bool m_logging;
};

}}

#endif
