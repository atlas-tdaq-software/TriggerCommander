/*
 * AutoPrescalerCommander.h
 *
 *  Created on: Oct 14, 2011
 *      Author: avolio
 */

#ifndef DAQ_TRIGGER_AUTOPRESCALERCOMMANDER_H_
#define DAQ_TRIGGER_AUTOPRESCALERCOMMANDER_H_

#include <ipc/partition.h>

#include <boost/noncopyable.hpp>
#include <string>

namespace daq { namespace trigger {

/**
 * Simple class allowing to send commands (via IPC/CORBA) to any application implementing the TRIGGER::autoprescaler IDL interface.
 */
class AutoPrescalerCommander: public boost::noncopyable {
    public:
        /**
         * Constructor.
         *
         * \param partitionName The name of the IPC partition the application to send commands to belongs to
         * \param applicationName The name the autoprescaler application is published in IPC
         * \param requestorID An identifier for (this) the application sending the commands
         */
        AutoPrescalerCommander(const std::string& partitionName,
                               const std::string& applicationName,
                               const std::string& requestorID);

        /**
         * Destructor.
         */
        ~AutoPrescalerCommander();

        /**
         * It sends the autoprescaler application the command to hold the automatic prescaling actions
         *
         * \throw daq::trigger::CommandFailed The remote command execution failed
         * \throw daq::trigger::CorbaException The remote application could not be contacted
         * \throw daq::trigger::IPCLookup The application could not be found in IPC
         */
        void hold();

        /**
         * It sends the autoprescaler application the command to resume the automatic prescaling actions
         *
         * \throw daq::trigger::CommandFailed The remote command execution failed
         * \throw daq::trigger::CorbaException The remote application could not be contacted
         * \throw daq::trigger::IPCLookup The application could not be found in IPC
         */
        void resume();

    private:
        IPCPartition m_partition;
        const std::string m_app_name;
        const std::string m_req_name;
        const std::string m_user_name;
};

}}

#endif /* AUTOPRESCALERCOMMANDER_H_ */
