// Wrapper of the trigger commander stub
// Author: Giovanna.Lehmann@cern.ch

#ifndef DAQ_TRIGGER_TRIGGERCOMMANDER_H_
#define DAQ_TRIGGER_TRIGGERCOMMANDER_H_

#include "TriggerCommander/HoldTriggerInfo.h"

#include <ipc/partition.h>

#include <string>
#include <sys/types.h>


namespace daq { namespace trigger {

/**
 * Class providing the functionality to send commands to a Master Trigger remote application.
 */
class TriggerCommander {
    public:
        /**
         * Constructor
         *
         * @param partition The IPC partition
         * @param triggerName The name of the application acting as a Master Trigger
         */
        TriggerCommander(IPCPartition partition, const std::string & triggerName);

        ~TriggerCommander();

        /**
         * \brief stops the master trigger in a partition
         * @param dm String-encoded bit mask identifying the detectors causing the trigger to be held;
         *           default value is empty string (i.e., the trigger-on-hold is not related to any specific detector)
         * \return Data structure holding information about the current ECR value and the last valid extended L1 ID
         * \throw daq::trigger::CommandFailed The remote command could not be properly executed
         * \throw daq::trigger::CorbaException An error occurred at the CORBA layer
         * \throw daq::trigger::IPCLookup The MasterTrigger could not be found in IPC
         */
        HoldTriggerInfo hold(const std::string& dm = "");

        /**
         * \brief resumes the master trigger in a partition
         * @param dm String-encoded bit mask identifying the detectors causing the trigger to be resumed;
         *           default value is empty string (i.e., the resume-trigger is not related to any specific detector)
         * \throw daq::trigger::CommandFailed The remote command could not be properly executed
         * \throw daq::trigger::CorbaException An error occurred at the CORBA layer
         * \throw daq::trigger::IPCLookup The MasterTrigger could not be found in IPC
         */
        void resume(const std::string& dm = "");

        /**
         * \brief changes L1 and HLT prescales
         * \param l1p L1 prescale key
         * \param hltp HLT prescale key
         * \throw daq::trigger::CommandFailed The remote command could not be properly executed
         * \throw daq::trigger::CorbaException An error occurred at the CORBA layer
         * \throw daq::trigger::IPCLookup The MasterTrigger could not be found in IPC
         */
        void setPrescales(uint32_t l1p, uint32_t hltp);

        /**
         * \brief changes L1 prescale key
         * \param l1p prescale key
         * \throw daq::trigger::CommandFailed The remote command could not be properly executed
         * \throw daq::trigger::CorbaException An error occurred at the CORBA layer
         * \throw daq::trigger::IPCLookup The MasterTrigger could not be found in IPC
         */
        void setL1Prescales(uint32_t l1p);

        /**
         * \brief changes HLT prescale key from a predefined LB
         * \param hltp prescale key
         * \throw daq::trigger::CommandFailed The remote command could not be properly executed
         * \throw daq::trigger::CorbaException An error occurred at the CORBA layer
         * \throw daq::trigger::IPCLookup The MasterTrigger could not be found in IPC
         */
        void setHLTPrescales(uint32_t hltp);

        /**
         * \brief changes the Luminosity block interval
         * \param seconds The Luminosity block interval in seconds
         * \throw daq::trigger::CommandFailed The remote command could not be properly executed
         * \throw daq::trigger::CorbaException An error occurred at the CORBA layer
         * \throw daq::trigger::IPCLookup The MasterTrigger could not be found in IPC
         */
        void setLumiBlockInterval(uint32_t seconds);

        /**
         * \brief changes the minimum length of the Luminosity block interval
         * \param seconds The minimum length of the Luminosity block interval in seconds
         * \throw daq::trigger::CommandFailed The remote command could not be properly executed
         * \throw daq::trigger::CorbaException An error occurred at the CORBA layer
         * \throw daq::trigger::IPCLookup The MasterTrigger could not be found in IPC
         */
        void setMinLumiBlockLength(uint32_t seconds);

        /**
         * \brief increases Luminosity block number
         * \param runno run number
         * \throw daq::trigger::CommandFailed The remote command could not be properly executed
         * \throw daq::trigger::CorbaException An error occurred at the CORBA layer
         * \throw daq::trigger::IPCLookup The MasterTrigger could not be found in IPC
         */
        void increaseLumiBlock(uint32_t runno);

        /**
         * \brief changes bunch group key
         * \param bg bunch group key
         * \throw daq::trigger::CommandFailed The remote command could not be properly executed
         * \throw daq::trigger::CorbaException An error occurred at the CORBA layer
         * \throw daq::trigger::IPCLookup The MasterTrigger could not be found in IPC
         */
        void setBunchGroup(uint32_t bg);

        /**
         * \brief Performs the update of an arbitrary condition
         * \param folderIndex The folder index
         * \param lb Luminosity block number
         * \throw daq::trigger::CommandFailed The remote command could not be properly executed
         * \throw daq::trigger::CorbaException An error occurred at the CORBA layer
         * \throw daq::trigger::IPCLookup The MasterTrigger could not be found in IPC
         */
        void setConditionsUpdate(uint32_t folderIndex, uint32_t lb);

        /**
         * \brief Changes L1 and HLT prescale and bunch group keys
         * \param l1p The L1 prescale key
         * \param hltp The HLT prescale key
         * \param bg The bunch group key
         * \throw daq::trigger::CommandFailed The remote command could not be properly executed
         * \throw daq::trigger::CorbaException An error occurred at the CORBA layer
         * \throw daq::trigger::IPCLookup The MasterTrigger could not be found in IPC
         */
        void setPrescalesAndBunchgroup(uint32_t l1p, uint32_t hltp, uint32_t bg);

        /**
         * \brief checks whether the master trigger is published on IPC
         * \return bool
         */
        bool exists();

    private:
        TriggerCommander();
        TriggerCommander(const TriggerCommander&);

        std::string userCredentials() const;

        IPCPartition m_partition;
        const std::string m_name;
        const std::string m_username;
        const std::string m_hostname;
};

}}

#endif /* DAQ_TRIGGER_TRIGGERCOMMANDER_H_ */
