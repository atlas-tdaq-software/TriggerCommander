/*
 * HoldTriggerInfo.h
 *
 *  Created on: Oct 10, 2022
 *      Author: avolio
 */

#ifndef DAQ_TRIGGER_HOLDTRIGGERINFO_H_
#define DAQ_TRIGGER_HOLDTRIGGERINFO_H_

#include <cstdint>

namespace daq { namespace trigger {
    /**
     * Data structure returned when the Master Trigger is asked to hold the trigger.
     */
    struct HoldTriggerInfo {
        uint8_t  ecrCounter;         ///< \brief The current ECR counter value
        uint32_t lastValidEL1ID;     ///< \brief The last @em valid extended L1 ID
    };
}}


#endif /* DAQ_TRIGGER_HOLDTRIGGERINFO_H_ */
