package TRIGGER;

import java.util.logging.*;

import daq.tokens.AcquireTokenException;
import daq.tokens.JWToken.MODE;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Wrapper class to send commands to the partition's Master Trigger
 * 
 * @author Giovanna Lehmann Miotto
 */
public class TriggerCommander {
    
    /**
     * Class holding information returned by the {@link TriggerCommander#hold()} and {@link TriggerCommander#hold(String)}
     * methods.
     */
    public static class HoldTriggerInfo {
        final private int ecrCounter;
        final private long exendedL1ID;
        
        private HoldTriggerInfo(final int ecrCounter, final long extendedL1ID) {
            this.ecrCounter = ecrCounter;
            this.exendedL1ID = extendedL1ID;
        }

        /**
         * It returns the current ECR value
         * 
         * @return the ecrCounter The Current ECR value
         */
        public int getEcrCounter() {
            return this.ecrCounter;
        }

        /**
         * It returns the last valid extended L1ID
         * 
         * @return the exendedL1ID The last valid extended L1ID
         */
        public long getExendedL1ID() {
            return this.exendedL1ID;
        }
    }
    
    /**
     * Constructor.
     * 
     * @param partitionName The name of the partition
     * @param triggerName The name of the Master Trigger application (i.e., the name used to publish in IPC)
     */
    public TriggerCommander(String partitionName, String triggerName) {
        this.partition = new ipc.Partition(partitionName);
        this.name = triggerName;
        this.userName = System.getProperty("user.name");
        this.hostName = TriggerCommander.getHostName();
    }

    /**
     * Constructor.
     * 
     * @param thePartition The IPC partition
     * @param triggerName The name of the Master Trigger application (i.e., the name used to publish in IPC)
     */
    public TriggerCommander(ipc.Partition thePartition, String triggerName) {
        this.partition = thePartition;
        this.name = triggerName;
        this.userName = System.getProperty("user.name");
        this.hostName = TriggerCommander.getHostName();
    }

    /**
     * It asks the Master Trigger application to hold the trigger
     * 
     * @return A data structure holding the current ECR value and the last valid extended L1ID
     * @throws TRIGGER.CommandExecutionFailed The trigger could be held
     */
    public HoldTriggerInfo hold() throws TRIGGER.CommandExecutionFailed {
        return this.hold("");
    }

    /**
     * It asks the Master Trigger application to hold the trigger
     * 
     * @param dm String-encoded bit mask identifying the detectors causing the trigger to be held;
     *           an empty string means that the trigger-on-hold is not related to any specific detector
     * @return A data structure holding the current ECR value and the last valid extended L1ID
     * @throws TRIGGER.CommandExecutionFailed The trigger could be held
     */
    public HoldTriggerInfo hold(final String dm) throws TRIGGER.CommandExecutionFailed {
        TRIGGER.commander ctrl = null;
        final HoldTriggerInfoHolder trgInfoHolder;

        try {
            logger.info("Lookup of controller " + this.name);
            ctrl = this.partition.lookup(TRIGGER.commander.class, this.name);

            logger.info("Invoke remote command send HOLD");
            trgInfoHolder = new HoldTriggerInfoHolder();
            ctrl.hold(this.getSenderContext(), dm, trgInfoHolder);
            logger.info("Success");

            // Update IS
            try {
                rc.TriggerStateNamed tt = new rc.TriggerStateNamed(this.partition, this.triggerStateServerName);
                tt.triggerStatus = rc.TriggerStateNamed.TriggerStatus.TriggerOnHold;
                tt.checkin();
            }
            catch(final Exception ex) {
                logger.severe("Failed updating the trigger state in IS after holding the trigger: " + ex);
            }

            return new HoldTriggerInfo(Byte.toUnsignedInt(trgInfoHolder.value.ecrCounter), 
                                       Integer.toUnsignedLong(trgInfoHolder.value.lastValidEL1ID));
        }
        catch(final AcquireTokenException ex) {
            throw new TRIGGER.CommandExecutionFailed("Cannot retrieve user credentials: " + ex);
        }
        catch(final ipc.InvalidObjectException ex) {
            throw new TRIGGER.CommandExecutionFailed("The \"" + this.name + "\" Master Trigger is not published in IPC: " + ex);
        }
        catch(final ipc.InvalidPartitionException ex) {
            throw new TRIGGER.CommandExecutionFailed("The IPC partition server cannot be reached: " + ex);
        }
        catch(final TRIGGER.CommandExecutionFailed ex) {
            throw ex;
        }
        catch(final Exception ex) {
            throw new TRIGGER.CommandExecutionFailed(ex.toString());
        }
    }
    
    /**
     * It asks the Master Trigger application to resume the trigger
     * 
     * @throws TRIGGER.CommandExecutionFailed The trigger could not be resumed
     */
    public void resume() throws TRIGGER.CommandExecutionFailed {
        this.resume("");
    }
    
    /**
     * It asks the Master Trigger application to resume the trigger
     * 
     * @param dm String-encoded bit mask identifying the detectors causing the trigger to be resumed;
     *           an empty string means that the resume-trigger is not related to any specific detector
     *
     * @throws TRIGGER.CommandExecutionFailed The trigger could not be resumed
     */
    public void resume(final String dm) throws TRIGGER.CommandExecutionFailed {
        TRIGGER.commander ctrl = null;

        try {
            logger.info("Lookup of controller " + this.name);
            ctrl = this.partition.lookup(TRIGGER.commander.class, this.name);
            logger.info("Invoke remote command send RESUME");
            ctrl.resume(this.getSenderContext(), dm);
            logger.info("Success");

            // Update IS
            try {
                rc.TriggerStateNamed tt = new rc.TriggerStateNamed(this.partition, this.triggerStateServerName);
                tt.triggerStatus = rc.TriggerStateNamed.TriggerStatus.TriggerActive;
                tt.checkin();
            }
            catch(final Exception ex) {
                logger.severe("Failed updating the trigger state in IS after resuming the trigger: " + ex);
            }
        }
        catch(final AcquireTokenException ex) {
            throw new TRIGGER.CommandExecutionFailed("Cannot retrieve user credentials: " + ex);
        }
        catch(final ipc.InvalidObjectException ex) {
            throw new TRIGGER.CommandExecutionFailed("The \"" + this.name + "\" Master Trigger is not published in IPC: " + ex);
        }
        catch(final ipc.InvalidPartitionException ex) {
            throw new TRIGGER.CommandExecutionFailed("The IPC partition server cannot be reached: " + ex);
        }
        catch(final TRIGGER.TriggerNotResumed ex) {
            logger.warning("The Master Trigger refused to resume the trigger: " + ex.reason);
        }
        catch(final TRIGGER.CommandExecutionFailed ex) {
            throw ex;
        }
        catch(final Exception ex) {
            throw new TRIGGER.CommandExecutionFailed(ex.toString());
        }
    }

    /**
     * It asks the Master Trigger application to set the trigger prescale configuration
     * 
     * @param l1p The L1 prescale key
     * @param hltp The HLT prescale key
     * 
     * @throws TRIGGER.CommandExecutionFailed Some error occurred while setting up the trigger configuration
     */
    public void setPrescales(int l1p, int hltp) throws TRIGGER.CommandExecutionFailed {
        TRIGGER.commander ctrl = null;

        try {
            logger.info("Lookup of controller " + this.name);
            ctrl = this.partition.lookup(TRIGGER.commander.class, this.name);
            logger.info("Invoke remote command send SETPRESCALES");
            ctrl.setPrescales(this.getSenderContext(), l1p, hltp);
            logger.info("Success");
        }
        catch(final AcquireTokenException ex) {
            throw new TRIGGER.CommandExecutionFailed("Cannot retrieve user credentials: " + ex);
        }
        catch(final ipc.InvalidObjectException ex) {
            throw new TRIGGER.CommandExecutionFailed("The \"" + this.name + "\" Master Trigger is not published in IPC: " + ex);
        }
        catch(final ipc.InvalidPartitionException ex) {
            throw new TRIGGER.CommandExecutionFailed("The IPC partition server cannot be reached: " + ex);
        }
        catch(final TRIGGER.CommandExecutionFailed ex) {
            throw ex;
        }
        catch(final Exception ex) {
            throw new TRIGGER.CommandExecutionFailed(ex.toString());
        }
    }

    /**
     * It asks the Master Trigger application to set the L1 prescale configuration
     * 
     * @param l1p The L1 prescale key
     * 
     * @throws TRIGGER.CommandExecutionFailed Some error occurred while setting up the trigger configuration
     */
    public void setL1Prescales(int l1p) throws TRIGGER.CommandExecutionFailed {
        TRIGGER.commander ctrl = null;

        try {
            logger.info("Lookup of controller " + this.name);
            ctrl = this.partition.lookup(TRIGGER.commander.class, this.name);
            logger.info("Invoke remote command send SETL1PRESCALES");
            ctrl.setL1Prescales(this.getSenderContext(), l1p);
            logger.info("Success");
        }
        catch(final AcquireTokenException ex) {
            throw new TRIGGER.CommandExecutionFailed("Cannot retrieve user credentials: " + ex);
        }
        catch(final ipc.InvalidObjectException ex) {
            throw new TRIGGER.CommandExecutionFailed("The \"" + this.name + "\" Master Trigger is not published in IPC: " + ex);
        }
        catch(final ipc.InvalidPartitionException ex) {
            throw new TRIGGER.CommandExecutionFailed("The IPC partition server cannot be reached: " + ex);
        }
        catch(final TRIGGER.CommandExecutionFailed ex) {
            throw ex;
        }
        catch(final Exception ex) {
            throw new TRIGGER.CommandExecutionFailed(ex.toString());
        }
    }

    /**
     * It asks the Master Trigger application to set the HLT prescale configuration
     * 
     * @param hltp The HLT prescale key
     * 
     * @throws TRIGGER.CommandExecutionFailed Some error occurred while setting up the trigger configuration
     */
    public void setHLTPrescales(int hltp) throws TRIGGER.CommandExecutionFailed {
        TRIGGER.commander ctrl = null;

        try {
            logger.info("Lookup of controller " + this.name);
            ctrl = this.partition.lookup(TRIGGER.commander.class, this.name);
            logger.info("Invoke remote command send SETHLTPRESCALES");
            ctrl.setHLTPrescales(this.getSenderContext(), hltp);
            logger.info("Success");
        }
        catch(final AcquireTokenException ex) {
            throw new TRIGGER.CommandExecutionFailed("Cannot retrieve user credentials: " + ex);
        }
        catch(final ipc.InvalidObjectException ex) {
            throw new TRIGGER.CommandExecutionFailed("The \"" + this.name + "\" Master Trigger is not published in IPC: " + ex);
        }
        catch(final ipc.InvalidPartitionException ex) {
            throw new TRIGGER.CommandExecutionFailed("The IPC partition server cannot be reached: " + ex);
        }
        catch(final TRIGGER.CommandExecutionFailed ex) {
            throw ex;
        }
        catch(final Exception ex) {
            throw new TRIGGER.CommandExecutionFailed(ex.toString());
        }
    }

    /**
     * It asks the Master Trigger application to set the bunch group configuration
     * 
     * @param bg The Bunch Group key
     * 
     * @throws TRIGGER.CommandExecutionFailed Some error occurred while setting up the trigger configuration
     */
    public void setBunchGroup(int bg) throws TRIGGER.CommandExecutionFailed {
        TRIGGER.commander ctrl = null;

        try {
            logger.info("Lookup of controller " + this.name);
            ctrl = this.partition.lookup(TRIGGER.commander.class, this.name);
            logger.info("Invoke remote command send SETBUNCHGROUP");
            ctrl.setBunchGroup(this.getSenderContext(), bg);
            logger.info("Success");
        }
        catch(final AcquireTokenException ex) {
            throw new TRIGGER.CommandExecutionFailed("Cannot retrieve user credentials: " + ex);
        }
        catch(final ipc.InvalidObjectException ex) {
            throw new TRIGGER.CommandExecutionFailed("The \"" + this.name + "\" Master Trigger is not published in IPC: " + ex);
        }
        catch(final ipc.InvalidPartitionException ex) {
            throw new TRIGGER.CommandExecutionFailed("The IPC partition server cannot be reached: " + ex);
        }
        catch(final TRIGGER.CommandExecutionFailed ex) {
            throw ex;
        }
        catch(final Exception ex) {
            throw new TRIGGER.CommandExecutionFailed(ex.toString());
        }
    }

    /**
     * It aks the Master Trigger application to set the duration of a Luminosity Block
     * 
     * @param seconds The Luminosity Block's duration (in seconds)
     * 
     * @throws TRIGGER.CommandExecutionFailed The command could not be executed
     */
    public void setLumiBlockInterval(int seconds) throws TRIGGER.CommandExecutionFailed {
        TRIGGER.commander ctrl = null;

        try {
            logger.info("Lookup of controller " + this.name);
            ctrl = this.partition.lookup(TRIGGER.commander.class, this.name);
            logger.info("Invoke remote command send SETLUMIBLOCKLEN");
            ctrl.setLumiBlockInterval(this.getSenderContext(), seconds);
            logger.info("Success");
        }
        catch(final AcquireTokenException ex) {
            throw new TRIGGER.CommandExecutionFailed("Cannot retrieve user credentials: " + ex);
        }
        catch(final ipc.InvalidObjectException ex) {
            throw new TRIGGER.CommandExecutionFailed("The \"" + this.name + "\" Master Trigger is not published in IPC: " + ex);
        }
        catch(final ipc.InvalidPartitionException ex) {
            throw new TRIGGER.CommandExecutionFailed("The IPC partition server cannot be reached: " + ex);
        }
        catch(final TRIGGER.CommandExecutionFailed ex) {
            throw ex;
        }
        catch(final Exception ex) {
            throw new TRIGGER.CommandExecutionFailed(ex.toString());
        }
    }
    
    /**
     * It aks the Master Trigger application to set the minimum duration of a Luminosity Block
     * 
     * @param seconds The Luminosity Block's minimum duration (in seconds)
     * 
     * @throws TRIGGER.CommandExecutionFailed The command could not be executed
     */
    public void setMinBlockLength(int seconds) throws TRIGGER.CommandExecutionFailed {
        TRIGGER.commander ctrl = null;

        try {
            logger.info("Lookup of controller " + this.name);
            ctrl = this.partition.lookup(TRIGGER.commander.class, this.name);
            logger.info("Invoke remote command send SETMINLUMIBLOCKLEN");
            ctrl.setMinLumiBlockLength(this.getSenderContext(), seconds);
            logger.info("Success");
        }
        catch(final AcquireTokenException ex) {
            throw new TRIGGER.CommandExecutionFailed("Cannot retrieve user credentials: " + ex);
        }
        catch(final ipc.InvalidObjectException ex) {
            throw new TRIGGER.CommandExecutionFailed("The \"" + this.name + "\" Master Trigger is not published in IPC: " + ex);
        }
        catch(final ipc.InvalidPartitionException ex) {
            throw new TRIGGER.CommandExecutionFailed("The IPC partition server cannot be reached: " + ex);
        }
        catch(final TRIGGER.CommandExecutionFailed ex) {
            throw ex;
        }
        catch(final Exception ex) {
            throw new TRIGGER.CommandExecutionFailed(ex.toString());
        }
    }

    /**
     * It asks the Master Trigger application to increase the current Luminosity Block number by one
     * 
     * @param runno The current Run Number
     * 
     * @throws TRIGGER.CommandExecutionFailed The command could not be executed
     */
    public void increaseLumiBlock(int runno) throws TRIGGER.CommandExecutionFailed {
        TRIGGER.commander ctrl = null;

        try {
            logger.info("Lookup of controller " + this.name);
            ctrl = this.partition.lookup(TRIGGER.commander.class, this.name);
            logger.info("Invoke remote command send INCREASELUMIBLOCK");
            ctrl.increaseLumiBlock(this.getSenderContext(), runno);
            logger.info("Success");
        }
        catch(final AcquireTokenException ex) {
            throw new TRIGGER.CommandExecutionFailed("Cannot retrieve user credentials: " + ex);
        }
        catch(final ipc.InvalidObjectException ex) {
            throw new TRIGGER.CommandExecutionFailed("The \"" + this.name + "\" Master Trigger is not published in IPC: " + ex);
        }
        catch(final ipc.InvalidPartitionException ex) {
            throw new TRIGGER.CommandExecutionFailed("The IPC partition server cannot be reached: " + ex);
        }
        catch(final TRIGGER.CommandExecutionFailed ex) {
            throw ex;
        }
        catch(final Exception ex) {
            throw new TRIGGER.CommandExecutionFailed(ex.toString());
        }
    }
    
    /**
     * It asks the Master Trigger application to update some condition data
     * 
     * @param folderIndex The folder index for the new conditions
     * 
     * @throws TRIGGER.CommandExecutionFailed The command could not be executed
     */
    public void setConditionsUpdate(int folderIndex, int lb) throws TRIGGER.CommandExecutionFailed {
        TRIGGER.commander ctrl = null;

        try {
            logger.info("Lookup of controller " + this.name);
            ctrl = this.partition.lookup(TRIGGER.commander.class, this.name);
            logger.info("Invoke remote command send SETCONDITIONSUPDATE");
            ctrl.setConditionsUpdate(this.getSenderContext(), folderIndex, lb);
            logger.info("Success");
        }
        catch(final AcquireTokenException ex) {
            throw new TRIGGER.CommandExecutionFailed("Cannot retrieve user credentials: " + ex);
        }
        catch(final ipc.InvalidObjectException ex) {
            throw new TRIGGER.CommandExecutionFailed("The \"" + this.name + "\" Master Trigger is not published in IPC: " + ex);
        }
        catch(final ipc.InvalidPartitionException ex) {
            throw new TRIGGER.CommandExecutionFailed("The IPC partition server cannot be reached: " + ex);
        }
        catch(final TRIGGER.CommandExecutionFailed ex) {
            throw ex;
        }
        catch(final Exception ex) {
            throw new TRIGGER.CommandExecutionFailed(ex.toString());
        }
    }

    /**
     * It asks the Master Trigger application to set the trigger prescales and bunch group configuration
     * 
     * @param l1p The L1 prescale key
     * @param hltp The HLT prescale key
     * @param bg The bunch group key
     * 
     * @throws TRIGGER.CommandExecutionFailed Some error occurred while setting up the trigger configuration
     */
    public void setPrescales(int l1p, int hltp, int bg) throws TRIGGER.CommandExecutionFailed {
        TRIGGER.commander ctrl = null;

        try {
            logger.info("Lookup of controller " + this.name);
            ctrl = this.partition.lookup(TRIGGER.commander.class, this.name);
            logger.info("Invoke remote command send SETPRESCALESANDBG");
            ctrl.setPrescalesAndBunchgroup(this.getSenderContext(), l1p, hltp, bg);
            logger.info("Success");
        }
        catch(final AcquireTokenException ex) {
            throw new TRIGGER.CommandExecutionFailed("Cannot retrieve user credentials: " + ex);
        }
        catch(final ipc.InvalidObjectException ex) {
            throw new TRIGGER.CommandExecutionFailed("The \"" + this.name + "\" Master Trigger is not published in IPC: " + ex);
        }
        catch(final ipc.InvalidPartitionException ex) {
            throw new TRIGGER.CommandExecutionFailed("The IPC partition server cannot be reached: " + ex);
        }
        catch(final TRIGGER.CommandExecutionFailed ex) {
            throw ex;
        }
        catch(final Exception ex) {
            throw new TRIGGER.CommandExecutionFailed(ex.toString());
        }
    }
        
    private static String getHostName() {
        try {
            return InetAddress.getLocalHost().getCanonicalHostName();
        }
        catch(final UnknownHostException ex) {
            return "unknown";
        }
    }
    
    private TRIGGER.SenderContext getSenderContext() throws AcquireTokenException {
        if(TriggerCommander.IS_TOKEN_ENABLED == false) {
            return new TRIGGER.SenderContext(this.userName, this.hostName);
        }
        
        return new TRIGGER.SenderContext(daq.tokens.JWToken.acquire(MODE.REUSE, "trg://" + this.partition.getName()), this.hostName);
    }
    
    private static final boolean IS_TOKEN_ENABLED = daq.tokens.JWToken.enabled();
    private final ipc.Partition partition;
    private final String name;
    private final String userName;
    private final String hostName;
    private final String triggerStateServerName = "RunCtrl.TriggerState";
    private final static Logger logger = Logger.getLogger("igui");
}
