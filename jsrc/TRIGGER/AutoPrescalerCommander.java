package TRIGGER;

import java.util.logging.Logger;


/**
 * Simple utility class to be used to send command (over IPC/CORBA) to remote object implementing the {@link TRIGGER.autoprescaler}
 * interface.
 */
public class AutoPrescalerCommander {
    private final ipc.Partition ipcPartition;
    private final String prescalerName;
    private final String sender;
    private final String userName = System.getProperty("user.name", "unknown");
    private final static Logger logger = Logger.getLogger("igui");

    /**
     * Constructor.
     * 
     * @param partitionName The name of the partition the remote object belongs to
     * @param prescalerName The name the remote object is published with
     * @param sender Identifier of the (this) sender application (e.g., OnlineExpertSystem, IGUI, ...)
     */
    public AutoPrescalerCommander(final String partitionName, final String prescalerName, final String sender) {
	this(new ipc.Partition(partitionName), prescalerName, sender);
    }

    /**
     * Constructor.
     * 
     * @param ipcPartition The IPC partition the remote object belongs to
     * @param prescalerName The name the remote object is published with
     * @param sender Identifier of the (this) sender application (e.g., OnlineExpertSystem, IGUI, ...)
     */
    public AutoPrescalerCommander(final ipc.Partition ipcPartition, final String prescalerName, final String sender) {
	this.ipcPartition = ipcPartition;
	this.prescalerName = prescalerName;
	this.sender = sender;
    }

    /**
     * It sends the 'hold' command to the remote object.
     * 
     * @throws CommandExecutionFailed The remote call failed.
     */
    public void hold() throws CommandExecutionFailed {
        TRIGGER.autoprescaler ap = null;
        try {
	    AutoPrescalerCommander.logger.info("Look up for " + this.prescalerName);
	    ap = this.ipcPartition.lookup(TRIGGER.autoprescaler.class, this.prescalerName);

	    AutoPrescalerCommander.logger.info("Sending remote command HOLD");
	    ap.hold(this.userName, this.sender);

	    AutoPrescalerCommander.logger.info("Success");
	}
	catch(final ipc.InvalidObjectException ex) {
	    throw new CommandExecutionFailed(this.prescalerName + " is not published in IPC: " + ex);
	}
        catch(final ipc.InvalidPartitionException ex) {
            throw new CommandExecutionFailed("The IPC partition cannot be reached: " + ex);
        }
	catch(final Exception ex) {
	    throw new CommandExecutionFailed(ex.toString());
	}
    }

    /**
     * It sends the 'resume' command to the remote object.
     * 
     * @throws CommandExecutionFailed The remote call failed.
     */
    public void resume() throws CommandExecutionFailed {
        TRIGGER.autoprescaler ap = null;
	try {
	    AutoPrescalerCommander.logger.info("Look up for " + this.prescalerName);
	    ap = this.ipcPartition.lookup(TRIGGER.autoprescaler.class, this.prescalerName);

	    AutoPrescalerCommander.logger.info("Sending remote command RESUME");
	    ap.resume(this.userName, this.sender);

	    AutoPrescalerCommander.logger.info("Success");
	}
	catch(final ipc.InvalidObjectException ex) {
	    throw new CommandExecutionFailed(this.prescalerName + " is not published in IPC: " + ex);
	}
	catch(final ipc.InvalidPartitionException ex) {
            throw new CommandExecutionFailed("The IPC partition cannot be reached: " + ex);
        }
	catch(final Exception ex) {
	    throw new CommandExecutionFailed(ex.toString());
	}
    }
}
