package TRIGGER;

/**
 * Class implementing the CORBA Servant functionality for the {@link TRIGGER.autoprescaler} interface.
 * <p>
 * Important: when this Servant is no more needed, the {@link #withdraw()} method must be called, otherwise the CORBA reference is kept in
 * IPC and the application may leak (the object will never be reclaimed by the garbage collector).
 */
public class AutoPrescalerServant extends ipc.NamedObject<TRIGGER.autoprescaler> implements TRIGGER.autoprescalerOperations {
    private final AutoPrescalerOperations ap;

    /**
     * Interface to be implemented to perform the requested {@link AutoPrescalerServant} operations.
     * <p>
     * Whenever a method of this interface throws an exception, it will be reported to the remote caller and wrapped in a
     * {@link CommandExecutionFailed} exception.
     */
    public static interface AutoPrescalerOperations {
	/**
	 * This method should perform the actions needed when the 'hold' command is received.
	 * 
	 * @param sender The command sender identifier
	 * @throws Exception Some error occurred executing the command
	 */
	public void hold(final String sender) throws Exception;

	/**
	 * This method should perform the actions needed when the 'resume' command is received.
	 * 
	 * @param sender The command sender identifier
	 * @throws Exception Some error occurred executing the command
	 */
	public void resume(final String sender) throws Exception;
    }

    /**
     * @param partitionName The name of the partition this Servant should be published to
     * @param name The name of this Servant (other applications will look for this Servant using <code>name</code> as an identifier)
     * @param ap The {@link AutoPrescalerOperations} object implementing the actions to be performed at any remote call
     * @throws ipc.InvalidPartitionException The partition this Servant should be published to does not exist
     */
    public AutoPrescalerServant(final String partitionName, final String name, final AutoPrescalerOperations ap)
	throws ipc.InvalidPartitionException
    {
	this(new ipc.Partition(partitionName), name, ap);
    }

    /**
     * Constructor: the Servant is created and published in IPC.
     * 
     * @param ipcPartition The partition this Servant should be published to
     * @param name The name of this Servant (other applications will look for this Servant using <code>name</code> as an identifier)
     * @param ap The {@link AutoPrescalerOperations} object implementing the actions to be performed at any remote call
     * @throws ipc.InvalidPartitionException The partition this Servant should be published to does not exist
     */
    public AutoPrescalerServant(final ipc.Partition ipcPartition, final String name, final AutoPrescalerOperations ap)
	throws ipc.InvalidPartitionException
    {
	super(ipcPartition, name);
	this.ap = ap;

	this.publish();
    }

    /**
     * This method is called when this Servant receives the corresponding CORBA remote call (as defined in the IDL interface)
     * <p>
     * It executes the actions defined in {@link AutoPrescalerOperations#hold(String)}
     * 
     * @param user Name of the user who sent the request
     * @param sender Name of the application the remote request is coming from
     * @throws CommandExecutionFailed Some error occurred during the command execution (this exception is reported to the caller)
     * @see TRIGGER.autoprescalerOperations#hold(java.lang.String, java.lang.String)
     */
    @Override
    public void hold(final String user, final String sender) throws CommandExecutionFailed {
	try {
	    this.ap.hold(sender);
	}
	catch(final Exception ex) {
	    throw new CommandExecutionFailed(ex.toString());
	}
    }

    /**
     * This method is called when this Servant receives the corresponding CORBA remote call (as defined in the IDL interface)
     * <p>
     * It executes the actions defined in {@link AutoPrescalerOperations#resume(String)}
     * 
     * @param user Name of the user who sent the request
     * @param sender Name of the application the remote request is coming from
     * @throws CommandExecutionFailed Some error occurred during the command execution (this exception is reported to the caller)
     * @see TRIGGER.autoprescalerOperations#resume(java.lang.String, java.lang.String)
     */
    @Override
    public void resume(final String user, final String sender) throws CommandExecutionFailed {
	try {
	    this.ap.resume(sender);
	}
	catch(final Exception ex) {
	    throw new CommandExecutionFailed(ex.toString());
	}
    }

    // public static void main(final String[] args) {
    // // args[0] = partition name
    // // args[1] = (this) application name
    // try {
    // final AutoPrescalerServant s = new AutoPrescalerServant(args[0], args[1], new AutoPrescalerOperations() {
    // @Override
    // public void hold(final String sender) {
    // System.out.println("Received the hold command from " + sender);
    // }
    //
    // @Override
    // public void resume(final String sender) {
    // System.out.println("Received the resume command from " + sender);
    // }
    // });
    //
    // synchronized(s) {
    // s.wait(15);
    // }
    //		
    // s.withdraw();
    // }
    // catch(final Exception ex) {
    // ex.printStackTrace();
    // }
    // }
}
