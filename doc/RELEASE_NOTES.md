# TriggerCommander

Package: [TriggerCommander](https://gitlab.cern.ch/atlas-tdaq-software/TriggerCommander)

**Implementations of the MasterTrigger interface**

An implementation of the `MasterTrigger` interface has to implement a new method:

```c++
class X : public MasterTrigger {
  ...

  void setPrescalesAndBunchgroup(uint32_t l1p, uint32_t hltp, uint32_t bg) override;
  ...
};
```

## tdaq-10-00-00

**Clients**

A breaking change (for clients) has been introduced in the 
`TriggerCommander/MasterTrigger.h` interface. The `hold()` method
has a new signature:

```c++
HoldTriggerInfo hold(const std::string& dm = "");
```

The `extended_lvl1id` boolean flag has been removed, and the method now returns
a data structure holding the current ECR value and the last valid extended L1ID.


**Implementations of the MasterTrigger interface**

An implementation of the `MasterTrigger` interface has to modify 
its overloaded method:

```c++
class X : public MasterTrigger {
  ...

  HoldTriggerInfo hold(const std::string& dm) override;
  ...
};
```

The `HoldTriggerInfo` data structure holds information about the current ECR value
and the last valid extended L1ID.

## tdaq-09-03-00

Implementation of the `daq tokens` mechanism.

## tdaq-09-01-01

**Clients**

A backward compatible change (for clients) has been introduced in the 
`TriggerCommander/MasterTrigger.h` interface. The `hold()` method
has been extended with a boolean flag `extended_lvl1id` which defaults
to `false`.

The method will return the last ECR as it did in the past. When the 
new flag is set to `true` it will instead return the full Level 1 ID
(from which the ECR can be extracted as the uppermost 8 bits).


**Implementations of the MasterTrigger interface**

An implementation of the `MasterTrigger` interface has to modify 
its overloaded method:

```c++
class X : public MasterTrigger {
  ...

  uint32_t hold(const std::string& dm, bool extended_lvl1id = false) override;
  ...
};
```
