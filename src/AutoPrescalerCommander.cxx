/*
 * AutoPrescalerCommander.cxx
 *
 *  Created on: Oct 14, 2011
 *      Author: avolio
 */

#include "TriggerCommander/AutoPrescalerCommander.h"
#include "TriggerCommander/Exceptions.h"

#include <trgCommander/trgCommander.hh>

#include <system/User.h>
#include <ers/Issue.h>


namespace daq { namespace trigger {

AutoPrescalerCommander::AutoPrescalerCommander(const std::string& partitionName,
                                               const std::string& appName,
                                               const std::string& requestorID) :
        m_partition(partitionName), m_app_name(appName), m_req_name(requestorID), m_user_name(System::User().name_safe())
{
}

AutoPrescalerCommander::~AutoPrescalerCommander() {
}

void AutoPrescalerCommander::resume() {
    try {
        TRIGGER::autoprescaler_var apRef = m_partition.lookup < TRIGGER::autoprescaler > (m_app_name.c_str());
        apRef->resume(m_user_name.c_str(), m_req_name.c_str());
    }
    catch(TRIGGER::CommandExecutionFailed& ex) {
        const std::string reason = std::string(ex._name()) + " (" + std::string(ex.reason) + ")";
        throw daq::trigger::CommandFailed(ERS_HERE, "resume auto-prescaler", reason.c_str());
    }
    catch(CORBA::Exception& ex) {
        throw daq::trigger::CorbaException(ERS_HERE, ex._name(), m_app_name.c_str());
    }
    catch(ipc::Exception& ex) {
        throw daq::trigger::IPCLookup(ERS_HERE, m_app_name.c_str(), ex);
    }
}

void AutoPrescalerCommander::hold() {
    try {
        TRIGGER::autoprescaler_var apRef = m_partition.lookup < TRIGGER::autoprescaler > (m_app_name.c_str());
        apRef->hold(m_user_name.c_str(), m_req_name.c_str());
    }
    catch(TRIGGER::CommandExecutionFailed& ex) {
        const std::string reason = std::string(ex._name()) + " (" + std::string(ex.reason) + ")";
        throw daq::trigger::CommandFailed(ERS_HERE, "hold auto-prescaler", reason.c_str());
    }
    catch(CORBA::Exception& ex) {
        throw daq::trigger::CorbaException(ERS_HERE, ex._name(), m_app_name.c_str());
    }
    catch(ipc::Exception& ex) {
        throw daq::trigger::IPCLookup(ERS_HERE, m_app_name.c_str(), ex);
    }
}

}
}
