// Author: Giovanna.Lehmann@cern.ch

#include "TriggerCommander/TriggerCommander.h"
#include "TriggerCommander/Exceptions.h"

#include <trgCommander/trgCommander.hh>

#include <rc/TriggerStateNamed.h>
#include <system/User.h>
#include <system/Host.h>
#include <ers/ers.h>
#include <daq_tokens/common.h>
#include <daq_tokens/acquire.h>
#include <daq_tokens/issues.h>


namespace daq { namespace trigger {

TriggerCommander::TriggerCommander(IPCPartition partition, const std::string& triggerName) :
        m_partition(partition), m_name(triggerName), m_username(System::User().name_safe()),
        m_hostname(System::LocalHost().full_name())
{
}

TriggerCommander::~TriggerCommander() {
}

HoldTriggerInfo TriggerCommander::hold(const std::string& dm) {
    ::TRIGGER::commander_var ctrl;
    ::TRIGGER::HoldTriggerInfo trgInfo;

    try {
        ctrl = m_partition.lookup < ::TRIGGER::commander > (m_name.c_str());

        ::TRIGGER::SenderContext ctx;
        ctx.userName = ::CORBA::string_dup(userCredentials().c_str());
        ctx.hostName = ::CORBA::string_dup(m_hostname.c_str());

        ctrl->hold(ctx, dm.c_str(), trgInfo);

        // Update IS
        try {
            TriggerStateNamed ts(m_partition, "RunCtrl.TriggerState");
            ts.triggerStatus = TriggerStateNamed::TriggerOnHold;
            ts.checkin();
        }
        catch(ers::Issue &e) {
            ers::warning(e);
        }
    }
    catch(::TRIGGER::CommandExecutionFailed& e) {
        const std::string reason = std::string(e._name()) + std::string(": ") + std::string(e.reason);
        throw daq::trigger::CommandFailed(ERS_HERE, "hold trigger", reason.c_str());
    }
    catch(CORBA::SystemException& e) {
        throw daq::trigger::CorbaException(ERS_HERE, e._name(), m_name.c_str());
    }
    catch(daq::ipc::Exception& e) {
        throw daq::trigger::IPCLookup(ERS_HERE, m_name.c_str(), e);
    }
    catch(daq::tokens::CannotAcquireToken& e) {
        throw daq::trigger::CommandFailed(ERS_HERE, "hold trigger", "Cannot acquire user token", e);
    }

    return (HoldTriggerInfo{trgInfo.ecrCounter, trgInfo.lastValidEL1ID});
}
void TriggerCommander::resume(const std::string& dm) {
    ::TRIGGER::commander_var ctrl;

    try {
        ctrl = m_partition.lookup < ::TRIGGER::commander > (m_name.c_str());

        ::TRIGGER::SenderContext ctx;
        ctx.userName = ::CORBA::string_dup(userCredentials().c_str());
        ctx.hostName = ::CORBA::string_dup(m_hostname.c_str());

        ctrl->resume(ctx, dm.c_str());

        // Update IS
        try {
            TriggerStateNamed ts(m_partition, "RunCtrl.TriggerState");
            ts.triggerStatus = TriggerStateNamed::TriggerActive;
            ts.checkin();
        }
        catch(ers::Issue& ex) {
            ers::warning(ex);
        }
    }
    catch(::TRIGGER::TriggerNotResumed& e) {
        ers::warning(daq::trigger::TriggerNotResumed(ERS_HERE, e.reason));
    }
    catch(::TRIGGER::CommandExecutionFailed& e) {
        const std::string reason = std::string(e._name()) + std::string(": ") + std::string(e.reason);
        throw daq::trigger::CommandFailed(ERS_HERE, "resume trigger", reason.c_str());
    }
    catch(CORBA::SystemException& e) {
        throw daq::trigger::CorbaException(ERS_HERE, e._name(), m_name.c_str());
    }
    catch(daq::ipc::Exception& e) {
        throw daq::trigger::IPCLookup(ERS_HERE, m_name.c_str(), e);
    }
    catch(daq::tokens::CannotAcquireToken& e) {
        throw daq::trigger::CommandFailed(ERS_HERE, "resume trigger", "Cannot acquire user token", e);
    }
}

void TriggerCommander::setPrescales(uint32_t l1p, uint32_t hltp) {
    ::TRIGGER::commander_var ctrl;

    try {
        ctrl = m_partition.lookup < ::TRIGGER::commander > (m_name.c_str());

        ::TRIGGER::SenderContext ctx;
        ctx.userName = ::CORBA::string_dup(userCredentials().c_str());
        ctx.hostName = ::CORBA::string_dup(m_hostname.c_str());

        ctrl->setPrescales(ctx, l1p, hltp);
    }
    catch(::TRIGGER::CommandExecutionFailed & e) {
        const std::string reason = std::string(e._name()) + std::string(": ") + std::string(e.reason);
        throw daq::trigger::CommandFailed(ERS_HERE, "set L1 and HLT prescales", reason.c_str());
    }
    catch(CORBA::SystemException & e) {
        throw daq::trigger::CorbaException(ERS_HERE, e._name(), m_name.c_str());
    }
    catch(daq::ipc::Exception& e) {
        throw daq::trigger::IPCLookup(ERS_HERE, m_name.c_str(), e);
    }
    catch(daq::tokens::CannotAcquireToken& e) {
        throw daq::trigger::CommandFailed(ERS_HERE, "set L1 and HLT prescales", "Cannot acquire user token", e);
    }
}

void TriggerCommander::setL1Prescales(uint32_t l1p) {
    ::TRIGGER::commander_var ctrl;

    try {
        ctrl = m_partition.lookup < ::TRIGGER::commander > (m_name.c_str());

        ::TRIGGER::SenderContext ctx;
        ctx.userName = ::CORBA::string_dup(userCredentials().c_str());
        ctx.hostName = ::CORBA::string_dup(m_hostname.c_str());

        ctrl->setL1Prescales(ctx, l1p);
    }
    catch(::TRIGGER::CommandExecutionFailed& e) {
        const std::string reason = std::string(e._name()) + std::string(": ") + std::string(e.reason);
        throw daq::trigger::CommandFailed(ERS_HERE, "set L1 prescales", reason.c_str());
    }
    catch(CORBA::SystemException& e) {
        throw daq::trigger::CorbaException(ERS_HERE, e._name(), m_name.c_str());
    }
    catch(daq::ipc::Exception& e) {
        throw daq::trigger::IPCLookup(ERS_HERE, m_name.c_str(), e);
    }
    catch(daq::tokens::CannotAcquireToken& e) {
        throw daq::trigger::CommandFailed(ERS_HERE, "set L1 prescales", "Cannot acquire user token", e);
    }
}

void TriggerCommander::setHLTPrescales(uint32_t hltp) {
    ::TRIGGER::commander_var ctrl;

    try {
        ctrl = m_partition.lookup < ::TRIGGER::commander > (m_name.c_str());

        ::TRIGGER::SenderContext ctx;
        ctx.userName = ::CORBA::string_dup(userCredentials().c_str());
        ctx.hostName = ::CORBA::string_dup(m_hostname.c_str());

        ctrl->setHLTPrescales(ctx, hltp);
    }
    catch(::TRIGGER::CommandExecutionFailed& e) {
        const std::string reason = std::string(e._name()) + std::string(": ") + std::string(e.reason);
        throw daq::trigger::CommandFailed(ERS_HERE, "set HLT prescales", reason.c_str());
    }
    catch(CORBA::SystemException & e) {
        throw daq::trigger::CorbaException(ERS_HERE, e._name(), m_name.c_str());
    }
    catch(daq::ipc::Exception& e) {
        throw daq::trigger::IPCLookup(ERS_HERE, m_name.c_str(), e);
    }
    catch(daq::tokens::CannotAcquireToken& e) {
        throw daq::trigger::CommandFailed(ERS_HERE, "set HLT prescales", "Cannot acquire user token", e);
    }
}

void TriggerCommander::increaseLumiBlock(uint32_t runno) {
    ::TRIGGER::commander_var ctrl;

    try {
        ctrl = m_partition.lookup < ::TRIGGER::commander > (m_name.c_str());

        ::TRIGGER::SenderContext ctx;
        ctx.userName = ::CORBA::string_dup(userCredentials().c_str());
        ctx.hostName = ::CORBA::string_dup(m_hostname.c_str());

        ctrl->increaseLumiBlock(ctx, runno);
    }
    catch(::TRIGGER::CommandExecutionFailed& e) {
        const std::string reason = std::string(e._name()) + std::string(": ") + std::string(e.reason);
        throw daq::trigger::CommandFailed(ERS_HERE, "increase luminosity block", reason.c_str());
    }
    catch(CORBA::SystemException& e) {
        throw daq::trigger::CorbaException(ERS_HERE, e._name(), m_name.c_str());
    }
    catch(daq::ipc::Exception& e) {
        throw daq::trigger::IPCLookup(ERS_HERE, m_name.c_str(), e);
    }
    catch(daq::tokens::CannotAcquireToken& e) {
        throw daq::trigger::CommandFailed(ERS_HERE, "increase luminosity block", "Cannot acquire user token", e);
    }
}

void TriggerCommander::setLumiBlockInterval(uint32_t seconds) {
    ::TRIGGER::commander_var ctrl;

    try {
        ctrl = m_partition.lookup < ::TRIGGER::commander > (m_name.c_str());

        ::TRIGGER::SenderContext ctx;
        ctx.userName = ::CORBA::string_dup(userCredentials().c_str());
        ctx.hostName = ::CORBA::string_dup(m_hostname.c_str());

        ctrl->setLumiBlockInterval(ctx, seconds);
    }
    catch(::TRIGGER::CommandExecutionFailed& e) {
        const std::string reason = std::string(e._name()) + std::string(": ") + std::string(e.reason);
        throw daq::trigger::CommandFailed(ERS_HERE, "set luminosity block interval", reason.c_str());
    }
    catch(CORBA::SystemException& e) {
        throw daq::trigger::CorbaException(ERS_HERE, e._name(), m_name.c_str());
    }
    catch(daq::ipc::Exception& e) {
        throw daq::trigger::IPCLookup(ERS_HERE, m_name.c_str(), e);
    }
    catch(daq::tokens::CannotAcquireToken& e) {
        throw daq::trigger::CommandFailed(ERS_HERE, "set luminosity block interval", "Cannot acquire user token", e);
    }
}

void TriggerCommander::setMinLumiBlockLength(uint32_t seconds) {
    ::TRIGGER::commander_var ctrl;

    try {
        ctrl = m_partition.lookup < ::TRIGGER::commander > (m_name.c_str());

        ::TRIGGER::SenderContext ctx;
        ctx.userName = ::CORBA::string_dup(userCredentials().c_str());
        ctx.hostName = ::CORBA::string_dup(m_hostname.c_str());

        ctrl->setMinLumiBlockLength(ctx, seconds);
    }
    catch(::TRIGGER::CommandExecutionFailed& e) {
        const std::string reason = std::string(e._name()) + std::string(": ") + std::string(e.reason);
        throw daq::trigger::CommandFailed(ERS_HERE, "set minimum luminosity block length", reason.c_str());
    }
    catch(CORBA::SystemException& e) {
        throw daq::trigger::CorbaException(ERS_HERE, e._name(), m_name.c_str());
    }
    catch(daq::ipc::Exception& e) {
        throw daq::trigger::IPCLookup(ERS_HERE, m_name.c_str(), e);
    }
    catch(daq::tokens::CannotAcquireToken& e) {
        throw daq::trigger::CommandFailed(ERS_HERE, "et minimum luminosity block length", "Cannot acquire user token", e);
    }
}

void TriggerCommander::setBunchGroup(uint32_t bg) {
    ::TRIGGER::commander_var ctrl;

    try {
        ctrl = m_partition.lookup < ::TRIGGER::commander > (m_name.c_str());

        ::TRIGGER::SenderContext ctx;
        ctx.userName = ::CORBA::string_dup(userCredentials().c_str());
        ctx.hostName = ::CORBA::string_dup(m_hostname.c_str());

        ctrl->setBunchGroup(ctx, bg);
    }
    catch(::TRIGGER::CommandExecutionFailed& e) {
        const std::string reason = std::string(e._name()) + std::string(": ") + std::string(e.reason);
        throw daq::trigger::CommandFailed(ERS_HERE, "set bunch group", reason.c_str());
    }
    catch(CORBA::SystemException& e) {
        throw daq::trigger::CorbaException(ERS_HERE, e._name(), m_name.c_str());
    }
    catch(daq::ipc::Exception& e) {
        throw daq::trigger::IPCLookup(ERS_HERE, m_name.c_str(), e);
    }
    catch(daq::tokens::CannotAcquireToken& e) {
        throw daq::trigger::CommandFailed(ERS_HERE, "set bunch group", "Cannot acquire user token", e);
    }
}

void TriggerCommander::setConditionsUpdate(uint32_t folderIndex, uint32_t lb) {
    ::TRIGGER::commander_var ctrl;

    try {
        ctrl = m_partition.lookup < ::TRIGGER::commander > (m_name.c_str());

        ::TRIGGER::SenderContext ctx;
        ctx.userName = ::CORBA::string_dup(userCredentials().c_str());
        ctx.hostName = ::CORBA::string_dup(m_hostname.c_str());

        ctrl->setConditionsUpdate(ctx, folderIndex, lb);
    }
    catch(::TRIGGER::CommandExecutionFailed& e) {
        const std::string reason = std::string(e._name()) + std::string(": ") + std::string(e.reason);
        throw daq::trigger::CommandFailed(ERS_HERE, "set conditions update", reason.c_str());
    }
    catch(CORBA::SystemException& e) {
        throw daq::trigger::CorbaException(ERS_HERE, e._name(), m_name.c_str());
    }
    catch(daq::ipc::Exception& e) {
        throw daq::trigger::IPCLookup(ERS_HERE, m_name.c_str(), e);
    }
    catch(daq::tokens::CannotAcquireToken& e) {
        throw daq::trigger::CommandFailed(ERS_HERE, "set conditions update", "Cannot acquire user token", e);
    }
}

void TriggerCommander::setPrescalesAndBunchgroup(uint32_t l1p, uint32_t hltp, uint32_t bg) {
    ::TRIGGER::commander_var ctrl;

    try {
        ctrl = m_partition.lookup < ::TRIGGER::commander > (m_name.c_str());

        ::TRIGGER::SenderContext ctx;
        ctx.userName = ::CORBA::string_dup(userCredentials().c_str());
        ctx.hostName = ::CORBA::string_dup(m_hostname.c_str());

        ctrl->setPrescalesAndBunchgroup(ctx, l1p, hltp, bg);
    }
    catch(::TRIGGER::CommandExecutionFailed & e) {
        const std::string reason = std::string(e._name()) + std::string(": ") + std::string(e.reason);
        throw daq::trigger::CommandFailed(ERS_HERE, "set prescales and bung group", reason.c_str());
    }
    catch(CORBA::SystemException & e) {
        throw daq::trigger::CorbaException(ERS_HERE, e._name(), m_name.c_str());
    }
    catch(daq::ipc::Exception& e) {
        throw daq::trigger::IPCLookup(ERS_HERE, m_name.c_str(), e);
    }
    catch(daq::tokens::CannotAcquireToken& e) {
        throw daq::trigger::CommandFailed(ERS_HERE, "set prescales and bunch group", "Cannot acquire user token", e);
    }
}

bool TriggerCommander::exists() {
    bool ret = false;

    try {
        m_partition.lookup < ::TRIGGER::commander > (m_name.c_str());
        ret = true;
    }
    catch(CORBA::SystemException & e) {
    }
    catch(daq::ipc::Exception& e) {
    }

    return ret;
}

// This can throw daq::tokens::CannotAcquireToken
std::string TriggerCommander::userCredentials() const {
    static const bool IS_TOKEN_ENABLED = daq::tokens::enabled();

    return (IS_TOKEN_ENABLED == false) ? m_username : daq::tokens::acquire(daq::tokens::Mode::Reuse, "trg://" + m_partition.name());
}

}}
