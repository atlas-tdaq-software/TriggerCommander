#include "TriggerCommander/CommandedTrigger.h"
#include "TriggerCommander/Exceptions.h"
#include "TriggerCommander/MasterTrigger.h"
#include "TriggerCommander/HoldTriggerInfo.h"

#include <AccessManager/xacml/impl/TriggerCommanderResource.h>
#include <AccessManager/client/RequestorInfo.h>
#include <AccessManager/client/ServerInterrogator.h>
#include <AccessManager/util/ErsIssues.h>
#include <ers/ers.h>
#include <system/User.h>
#include <ipc/partition.h>
#include <daq_tokens/common.h>
#include <daq_tokens/verify.h>
#include <daq_tokens/issues.h>

#include <cstdlib>

using tgr = daq::am::TriggerCommanderResource;

namespace daq { namespace trigger {

CommandedTrigger::CommandedTrigger(const IPCPartition& p, const std::string & name, MasterTrigger* const mt) :
        IPCNamedObject<POA_TRIGGER::commander, ::ipc::multi_thread>(p, name), mt_(mt),
        m_logging(std::getenv("TDAQ_MT_VERBOSE") != nullptr ? true : false)
{
    try {
        publish();
    }
    catch(daq::ipc::Exception& ex) {
        ers::error(ex);
    }

    System::User u;
    m_processOwner = u.name_safe();
}

CommandedTrigger::~CommandedTrigger() {
}

void CommandedTrigger::shutdown() {
    try {
        withdraw();
        _destroy(true);
    }
    catch(daq::ipc::Exception& e) {
    }
}

void CommandedTrigger::allowedByAM(const char * userName, const std::string& command, const std::string& args) {
    bool allowed = true;
    if((userName != m_processOwner)) {
        daq::am::TriggerCommanderResource amResource(command, this->partition().name());
        daq::am::ServerInterrogator amInterrogator;
        daq::am::RequestorInfo amReqInfo(userName);

        try {
            allowed = amInterrogator.isAuthorizationGranted(amResource, amReqInfo);
        }
        catch(daq::am::Exception & ex) {
            ers::error(ex);
            allowed = false;
        }
    }

    if(!allowed) {
        std::ostringstream txt;
        txt << "User " << userName << " is not allowed to send commands to the Trigger owned by " <<
                m_processOwner << ".";
        throw daq::trigger::InvalidCommand(ERS_HERE, command.c_str(), args.c_str(), txt.str().c_str());
    }
}

void CommandedTrigger::resume(const TRIGGER::SenderContext& sc, const char* dm) {
    try {
        const std::string& userName = verifyUserCredentials((const char*) sc.userName);

        if(m_logging == true) {
            ERS_LOG("Receiver command " << tgr::ACTION_VALUES[tgr::ACTION_ID_RESUME] << " from user "
                    << userName << " on host " << (const char *) sc.hostName);
        }

        allowedByAM(userName.c_str(), tgr::ACTION_VALUES[tgr::ACTION_ID_RESUME], dm); // throws InvalidCommand if not allowed
        mt_->resume(dm);
    }
    catch(daq::trigger::TriggerNotResumed& ex) {
        throw TRIGGER::TriggerNotResumed(ex.get_reason());
    }
    catch(ers::Issue& ex) {
        throw TRIGGER::CommandExecutionFailed(ex.what());
    }
    catch(std::exception& ex) {
        throw TRIGGER::CommandExecutionFailed(ex.what());
    }
    catch(...) {
        throw TRIGGER::CommandExecutionFailed("Unexpected error");
    }
}

void CommandedTrigger::hold(const TRIGGER::SenderContext& sc, const char* dm, TRIGGER::HoldTriggerInfo& triggerInfo) {
    try {
        const std::string& userName = verifyUserCredentials((const char*) sc.userName);

        if(m_logging == true) {
            ERS_LOG("Receiver command " << tgr::ACTION_VALUES[tgr::ACTION_ID_HOLD] << " from user "
                    << userName << " on host " << (const char *) sc.hostName);
        }

        allowedByAM(userName.c_str(), tgr::ACTION_VALUES[tgr::ACTION_ID_HOLD], dm); // throws InvalidCommand if not allowed

        const HoldTriggerInfo& trgInfo = mt_->hold(dm);
        triggerInfo.ecrCounter = trgInfo.ecrCounter;
        triggerInfo.lastValidEL1ID = trgInfo.lastValidEL1ID;
    }
    catch(ers::Issue& ex) {
        throw TRIGGER::CommandExecutionFailed(ex.what());
    }
    catch(std::exception& ex) {
        throw TRIGGER::CommandExecutionFailed(ex.what());
    }
    catch(...) {
        throw TRIGGER::CommandExecutionFailed("Unexpected error");
    }
}

void CommandedTrigger::setPrescales(const TRIGGER::SenderContext& sc,
                                    ::CORBA::ULong l1p,
                                    ::CORBA::ULong hltp)
{
    try {
        const std::string& userName = verifyUserCredentials((const char*) sc.userName);

        if(m_logging == true) {
            ERS_LOG("Receiver command " << tgr::ACTION_VALUES[tgr::ACTION_ID_SETPRESCALES] << " from user "
                    << userName << " on host " << (const char *) sc.hostName);
        }

        allowedByAM(userName.c_str(), tgr::ACTION_VALUES[tgr::ACTION_ID_SETPRESCALES], std::to_string(l1p) + " " + std::to_string(hltp)); // throws InvalidCommand if not allowed
        mt_->setPrescales(l1p, hltp);
    }
    catch(ers::Issue& ex) {
        throw TRIGGER::CommandExecutionFailed(ex.what());
    }
    catch(std::exception& ex) {
        throw TRIGGER::CommandExecutionFailed(ex.what());
    }
    catch(...) {
        throw TRIGGER::CommandExecutionFailed("Unexpected error");
    }
}

void CommandedTrigger::setL1Prescales(const TRIGGER::SenderContext& sc, ::CORBA::ULong l1p) {
    try {
        const std::string& userName = verifyUserCredentials((const char*) sc.userName);

        if(m_logging == true) {
            ERS_LOG("Receiver command " << tgr::ACTION_VALUES[tgr::ACTION_ID_SETL1PRESCALES] << " from user "
                    << userName << " on host " << (const char *) sc.hostName);
        }

        allowedByAM(userName.c_str(), tgr::ACTION_VALUES[tgr::ACTION_ID_SETL1PRESCALES], std::to_string(l1p)); // throws InvalidCommand if not allowed
        mt_->setL1Prescales(l1p);
    }
    catch(ers::Issue& ex) {
        throw TRIGGER::CommandExecutionFailed(ex.what());
    }
    catch(std::exception& ex) {
        throw TRIGGER::CommandExecutionFailed(ex.what());
    }
    catch(...) {
        throw TRIGGER::CommandExecutionFailed("Unexpected error");
    }
}

void CommandedTrigger::setHLTPrescales(const TRIGGER::SenderContext& sc, ::CORBA::ULong hltp) {
    try {
        const std::string& userName = verifyUserCredentials((const char*) sc.userName);

        if(m_logging == true) {
            ERS_LOG("Receiver command " << tgr::ACTION_VALUES[tgr::ACTION_ID_SETHLTPRESCALES] << " from user "
                    << userName << " on host " << (const char *) sc.hostName);
        }

        allowedByAM(userName.c_str(), tgr::ACTION_VALUES[tgr::ACTION_ID_SETHLTPRESCALES], std::to_string(hltp)); // throws InvalidCommand if not allowed
        mt_->setHLTPrescales(hltp);
    }
    catch(ers::Issue& ex) {
        throw TRIGGER::CommandExecutionFailed(ex.what());
    }
    catch(std::exception& ex) {
        throw TRIGGER::CommandExecutionFailed(ex.what());
    }
    catch(...) {
        throw TRIGGER::CommandExecutionFailed("Unexpected error");
    }
}

void CommandedTrigger::increaseLumiBlock(const TRIGGER::SenderContext& sc, ::CORBA::ULong runno) {
    try {
        const std::string& userName = verifyUserCredentials((const char*) sc.userName);

        if(m_logging == true) {
            ERS_LOG("Receiver command " << tgr::ACTION_VALUES[tgr::ACTION_ID_INCREASELUMIBLOCK] << " from user "
                    << userName << " on host " << (const char *) sc.hostName);
        }

        allowedByAM(userName.c_str(), tgr::ACTION_VALUES[tgr::ACTION_ID_INCREASELUMIBLOCK], std::to_string(runno)); // throws InvalidCommand if not allowed
        mt_->increaseLumiBlock(runno);
    }
    catch(ers::Issue& ex) {
        throw TRIGGER::CommandExecutionFailed(ex.what());
    }
    catch(std::exception& ex) {
        throw TRIGGER::CommandExecutionFailed(ex.what());
    }
    catch(...) {
        throw TRIGGER::CommandExecutionFailed("Unexpected error");
    }
}

void CommandedTrigger::setLumiBlockInterval(const TRIGGER::SenderContext& sc, ::CORBA::ULong seconds) {
    try {
        const std::string& userName = verifyUserCredentials((const char*) sc.userName);

        if(m_logging == true) {
            ERS_LOG("Receiver command " << tgr::ACTION_VALUES[tgr::ACTION_ID_SETLUMIBLOCKLEN] << " from user "
                    << userName << " on host " << (const char *) sc.hostName);
        }

        allowedByAM(userName.c_str(), tgr::ACTION_VALUES[tgr::ACTION_ID_SETLUMIBLOCKLEN], std::to_string(seconds)); // throws InvalidCommand if not allowed
        mt_->setLumiBlockInterval(seconds);
    }
    catch(ers::Issue& ex) {
        throw TRIGGER::CommandExecutionFailed(ex.what());
    }
    catch(std::exception& ex) {
        throw TRIGGER::CommandExecutionFailed(ex.what());
    }
    catch(...) {
        throw TRIGGER::CommandExecutionFailed("Unexpected error");
    }
}

void CommandedTrigger::setMinLumiBlockLength(const TRIGGER::SenderContext& sc, ::CORBA::ULong seconds) {
    try {
        const std::string& userName = verifyUserCredentials((const char*) sc.userName);

        if(m_logging == true) {
            ERS_LOG("Receiver command " << tgr::ACTION_VALUES[tgr::ACTION_ID_SETMINLUMIBLOCKLEN] << " from user "
                    << userName << " on host " << (const char *) sc.hostName);
        }

        allowedByAM(userName.c_str(), tgr::ACTION_VALUES[tgr::ACTION_ID_SETMINLUMIBLOCKLEN], std::to_string(seconds)); // throws InvalidCommand if not allowed
        mt_->setMinLumiBlockLength(seconds);
    }
    catch(ers::Issue& ex) {
        throw TRIGGER::CommandExecutionFailed(ex.what());
    }
    catch(std::exception& ex) {
        throw TRIGGER::CommandExecutionFailed(ex.what());
    }
    catch(...) {
        throw TRIGGER::CommandExecutionFailed("Unexpected error");
    }
}

void CommandedTrigger::setBunchGroup(const TRIGGER::SenderContext& sc, ::CORBA::ULong bg) {
    try {
        const std::string& userName = verifyUserCredentials((const char*) sc.userName);

        if(m_logging == true) {
            ERS_LOG("Receiver command " << tgr::ACTION_VALUES[tgr::ACTION_ID_SETBUNCHGROUP] << " from user "
                    << userName << " on host " << (const char *) sc.hostName);
        }

        allowedByAM(userName.c_str(), tgr::ACTION_VALUES[tgr::ACTION_ID_SETBUNCHGROUP], std::to_string(bg)); // throws InvalidCommand if not allowed
        mt_->setBunchGroup(bg);
    }
    catch(ers::Issue& ex) {
        throw TRIGGER::CommandExecutionFailed(ex.what());
    }
    catch(std::exception& ex) {
        throw TRIGGER::CommandExecutionFailed(ex.what());
    }
    catch(...) {
        throw TRIGGER::CommandExecutionFailed("Unexpected error");
    }
}

void CommandedTrigger::setConditionsUpdate(const TRIGGER::SenderContext& sc, ::CORBA::ULong folderIndex, ::CORBA::ULong lb)
{
    try {
        const std::string& userName = verifyUserCredentials((const char*) sc.userName);

        if(m_logging == true) {
            ERS_LOG("Receiver command " << tgr::ACTION_VALUES[tgr::ACTION_ID_SETCONDITIONSUPDATE] << " from user "
                    << userName << " on host " << (const char *) sc.hostName);
        }

        allowedByAM(userName.c_str(), tgr::ACTION_VALUES[tgr::ACTION_ID_SETCONDITIONSUPDATE], std::to_string(folderIndex) + " " + std::to_string(lb)); // throws InvalidCommand if not allowed
        mt_->setConditionsUpdate(folderIndex, lb);
    }
    catch(ers::Issue& ex) {
        throw TRIGGER::CommandExecutionFailed(ex.what());
    }
    catch(std::exception& ex) {
        throw TRIGGER::CommandExecutionFailed(ex.what());
    }
    catch(...) {
        throw TRIGGER::CommandExecutionFailed("Unexpected error");
    }
}

void CommandedTrigger::setPrescalesAndBunchgroup(const TRIGGER::SenderContext& sc, ::CORBA::ULong l1p, ::CORBA::ULong hltp, ::CORBA::ULong bg)
{
    try {
        const std::string& userName = verifyUserCredentials((const char*) sc.userName);

        if(m_logging == true) {
            ERS_LOG("Receiver command " << tgr::ACTION_VALUES[tgr::ACTION_ID_SETPRESCALESANDBUNCHGROUP] << " from user "
                    << userName << " on host " << (const char *) sc.hostName);
        }

        allowedByAM(userName.c_str(), tgr::ACTION_VALUES[tgr::ACTION_ID_SETPRESCALESANDBUNCHGROUP], std::to_string(l1p) + " " + std::to_string(hltp) + " " + std::to_string(bg)); // throws InvalidCommand if not allowed
        mt_->setPrescalesAndBunchgroup(l1p, hltp, bg);
    }
    catch(ers::Issue& ex) {
        throw TRIGGER::CommandExecutionFailed(ex.what());
    }
    catch(std::exception& ex) {
        throw TRIGGER::CommandExecutionFailed(ex.what());
    }
    catch(...) {
        throw TRIGGER::CommandExecutionFailed("Unexpected error");
    }
}

// This can throw daq::tokens::CannotVerifyToken
std::string CommandedTrigger::verifyUserCredentials(std::string_view userCredentials) const {
    static const bool IS_TOKEN_ENABLED = daq::tokens::enabled();

    return (IS_TOKEN_ENABLED == false) ? std::string(userCredentials) : daq::tokens::verify(userCredentials, "trg://" + m_partition.name()).get_subject();
}

}}
