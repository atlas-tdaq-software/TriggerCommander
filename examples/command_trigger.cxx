//
// Command sender to trigger
//
// Author: Giovanna.Lehmann@cern.ch

#include <TriggerCommander/TriggerCommander.h>
#include <TriggerCommander/HoldTriggerInfo.h>
#include <TriggerCommander/Exceptions.h>

#include "ers/ers.h"
#include "ipc/core.h"
#include "ipc/partition.h"

#include <boost/program_options.hpp>

#include <string>
#include <vector>
#include <cstdlib>

namespace po = boost::program_options;

int main(int argc, char ** argv) {
    // Initialize IPC
    try {
        IPCCore::init(argc, argv);
    }
    catch(daq::ipc::CannotInitialize& e) {
        ers::fatal(e);
        return EXIT_FAILURE;
    }
    catch(daq::ipc::AlreadyInitialized& e) {
        ers::warning(e);
    }

    // Parse the command line
    std::string partitionName = ((std::getenv("TDAQ_PARTITION") == nullptr) ? "" : std::getenv("TDAQ_PARTITION"));
    std::string controllerName;
    std::string command;
    std::vector<std::string> arguments;

    try {
        po::options_description desc("This program sends a command to the master trigger.");

        desc.add_options()
        ("partition,p", po::value<std::string>(&partitionName)->default_value(partitionName), "Name of the partition (TDAQ_PARTITION)")
        ("controller,n", po::value<std::string>(&controllerName), "Master trigger name")
        ("command,c", po::value<std::string>(&command),
                      "Command for the trigger: HOLD, RESUME, SETPRESCALES, SETL1PRESCALES, SETHLTPRESCALES, SETBUNCHGROUP, SETLUMIBLOCKLEN, SETMINLUMIBLOCKLEN, INCREASELUMIBLOCK, SETCONDITIONSUPDATE, SETPRESCALESANDBG")
        ("arguments", po::value<std::vector<std::string>>(&arguments), "The command options")
        ("help,h", "Print help message");

        po::positional_options_description p;
        p.add("arguments", -1);

        po::variables_map vm;
        po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
        po::notify(vm);

        if(vm.count("help")) {
            std::cout << desc << std::endl;
            return EXIT_SUCCESS;
        }

        if(partitionName.empty() || controllerName.empty() || command.empty()) {
            ers::fatal(daq::trigger::CmdlParsing(ERS_HERE, "the partition, controller and command are mandatory"));
            return EXIT_FAILURE;
        }
    }
    catch(boost::program_options::error& ex) {
        ers::fatal(daq::trigger::CmdlParsing(ERS_HERE, "low-level error from parsing library", ex));
        return EXIT_FAILURE;
    }

    IPCPartition p(partitionName);
    daq::trigger::TriggerCommander commander(p, controllerName);

    ERS_LOG("Sending " << command << " to " << controllerName << ".");

    try {
        const auto& argsSize = arguments.size();
        if(command == "HOLD") {
            const std::string dm = argsSize >= 1 ? arguments[0] : "";
            const daq::trigger::HoldTriggerInfo& i = commander.hold(dm);
            ERS_LOG("Trigger is now on hold, the ECR is " << i.ecrCounter
                     << " and the last valid extended L1ID is " << i.lastValidEL1ID);
        } else if(command == "RESUME") {
            const std::string dm = argsSize >= 1 ? arguments[0] : "";
            commander.resume(dm);
            ERS_LOG("Trigger is now resumed");
        } else if(command == "SETPRESCALES" && argsSize >= 2) {
            commander.setPrescales(std::stoul(arguments[0]), std::stoul(arguments[1]));
        } else if(command == "SETL1PRESCALES" && argsSize >= 1) {
            commander.setL1Prescales(std::stoul(arguments[0]));
        } else if(command == "SETHLTPRESCALES" && argsSize >= 1) {
            commander.setHLTPrescales(std::stoul(arguments[0]));
        } else if(command == "SETBUNCHGROUP" && argsSize >= 1) {
            commander.setBunchGroup(std::stoul(arguments[0]));
        } else if(command == "SETLUMIBLOCKLEN" && argsSize >= 1) {
            commander.setLumiBlockInterval(std::stoul(arguments[0]));
        } else if(command == "SETMINLUMIBLOCKLEN" && argsSize >= 1) {
            commander.setMinLumiBlockLength(std::stoul(arguments[0]));
        } else if(command == "INCREASELUMIBLOCK" && argsSize >= 1) {
            commander.increaseLumiBlock(std::stoul(arguments[0]));
        } else if(command == "SETCONDITIONSUPDATE" && argsSize >= 2) {
            commander.setConditionsUpdate(std::stoul(arguments[0]), std::stoul(arguments[1]));
        } else if(command == "SETPRESCALESANDBG" && argsSize >= 3) {
            commander.setPrescalesAndBunchgroup(std::stoul(arguments[0]), std::stoul(arguments[1]), std::stoul(arguments[2]));
        } else {
            ERS_LOG("Nothing done.");
            daq::trigger::CmdlParsing issue(ERS_HERE, "the command and its arguments are not valid");
            ers::fatal(issue);

            return EXIT_FAILURE;
        }
    }
    catch(ers::Issue &e) {
        ers::fatal(e);
        return EXIT_FAILURE;
    }
    catch(std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
