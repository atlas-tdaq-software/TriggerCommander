#include "TriggerCommander/AutoPrescalerCommander.h"
#include "TriggerCommander/Exceptions.h"

#include <ers/ers.h>
#include <ipc/core.h>
#include <ipc/partition.h>

#include <boost/program_options.hpp>

#include <string>
#include <cstdlib>

namespace po = boost::program_options;

int main(int argc, char ** argv) {
	// Initialize IPC
    try {
		IPCCore::init(argc, argv);
	}
	catch(daq::ipc::CannotInitialize& e) {
		ers::fatal(e);
		return EXIT_FAILURE;
	}
	catch(daq::ipc::AlreadyInitialized& e) {
		ers::warning(e);
	}

	// Parse the command line
	std::string partitionName = ((std::getenv("TDAQ_PARTITION") == nullptr) ? "" : std::getenv("TDAQ_PARTITION"));
	std::string controllerName;
	std::string command;

	try {
	    po::options_description desc("This program sends a command to the trigger auto-prescaler application.");

	    desc.add_options()("partition,p",  po::value<std::string>(&partitionName)->default_value(partitionName) , "Name of the partition (TDAQ_PARTITION)")
                          ("controller,n", po::value<std::string>(&controllerName)                              , "Auto-prescaler name")
                          ("command,c",    po::value<std::string>(&command)                                     , "Command for the auto-prescaler: HOLD, RESUME")
                          ("help,h"                                                                             , "Print help message");

	    po::variables_map vm;
	    po::store(po::parse_command_line(argc, argv, desc), vm);
	    po::notify(vm);

	    if(vm.count("help")) {
	        std::cout << desc << std::endl;
	        return EXIT_SUCCESS;
	    }

	    if(partitionName.empty() || controllerName.empty() || command.empty()) {
	        ers::fatal(daq::trigger::CmdlParsing(ERS_HERE, "the partition, controller and command are mandatory"));
	        return EXIT_FAILURE;
	    }
	}
	catch(boost::program_options::error& ex) {
	    ers::fatal(daq::trigger::CmdlParsing(ERS_HERE, "low-level error from the parsing librray", ex));
	    return EXIT_FAILURE;
	}

	daq::trigger::AutoPrescalerCommander commander(partitionName, controllerName, "command line");

	ERS_LOG("Sending " << command << " to " << controllerName << ".");

	try {
		if(command == "HOLD") {
			commander.hold();
			ERS_LOG("Trigger auto-prescaling is now on hold");
		} else if(command == "RESUME") {
			commander.resume();
			ERS_LOG("Trigger auto-prescaling is now resumed");
		} else {
			ERS_LOG("Nothing done.");
			daq::trigger::CmdlParsing issue(ERS_HERE, "the passed command is not valid");
			ers::fatal(issue);

			return EXIT_FAILURE;
		}
	}
	catch(ers::Issue &e) {
		ers::fatal(e);
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
