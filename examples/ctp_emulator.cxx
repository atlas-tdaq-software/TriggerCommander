//      Example of Master Trigger implementation
//
//      G. Lehmann Miotto, March 2010

#include "TriggerCommander/Exceptions.h"
#include "TriggerCommander/CommandedTrigger.h"
#include "TriggerCommander/MasterTrigger.h"
#include "TriggerCommander/HoldTriggerInfo.h"

#include <iostream>
#include <limits>
#include <boost/program_options.hpp>

#include "ers/ers.h"

namespace po = boost::program_options;

namespace daq { namespace trigger {

class MyMasterTrigger: public daq::trigger::MasterTrigger {
    public:
        MyMasterTrigger() : daq::trigger::MasterTrigger() {
            ERS_LOG("Instantiation of the implementation of the Master Trigger interface.");
        }

        virtual HoldTriggerInfo hold(const std::string&) override {
            ERS_LOG("Putting trigger on hold and retrieving last valid ECR");
            return HoldTriggerInfo{123, 456};
        }

        virtual void resume(const std::string&) override {
            ERS_LOG("Resuming trigger");
        }

        virtual void setPrescales(uint32_t l1p, uint32_t hltp) override {
            ERS_LOG("Setting L1 and HLT prescale keys to " << l1p << " and " << hltp);
        }

        virtual void setL1Prescales(uint32_t l1p) override {
            ERS_LOG("Setting L1 prescales key to " << l1p);
        }

        virtual void setHLTPrescales(uint32_t hltp) override {
            ERS_LOG("Switching to HLT prescales key " << hltp);
        }

        virtual void setLumiBlockInterval(uint32_t seconds) override {
            ERS_LOG("Setting the luminosity block interval to " << seconds << " seconds");
        }

        virtual void setMinLumiBlockLength(uint32_t seconds) override {
            ERS_LOG("Setting the minimum luminosity block length to " << seconds << " seconds");
        }

        virtual void increaseLumiBlock(uint32_t runno) override {
            ERS_LOG("Increasing the luminosity block number");
        }

        virtual void setBunchGroup(uint32_t bg) override {
            ERS_LOG("Setting bunch group key to " << bg);
        }

        virtual void setConditionsUpdate(uint32_t folderIndex, uint32_t lb) override {
            ERS_LOG("Setting conditions at folder index " << folderIndex << " from luminosity block " << lb);
        }

        virtual void setPrescalesAndBunchgroup(uint32_t l1p, uint32_t hltp, uint32_t bg) override {
            ERS_LOG("Setting prescale keys and bunch group to  " << l1p << ", " << hltp << " and " << bg);
        }
};

} }

int main(int argc, char **argv) {
    // Initialize IPC
    try {
        IPCCore::init(argc, argv);
    }
    catch(daq::ipc::CannotInitialize& e) {
        ers::fatal(e);
        abort();
    }
    catch(daq::ipc::AlreadyInitialized& e) {
        ers::warning(e);
    }

    // Command line parsing
    std::string name("");
    try {
        po::options_description desc("This program is a example data taking application.");

        desc.add_options()("name,n", po::value < std::string > (&name), "Name of the application")
                          ("help,h", "Print help message");

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        if(vm.count("help")) {
            std::cout << desc << std::endl;
            return EXIT_SUCCESS;
        }

        if(!vm.count("name")) {
            ers::error(daq::trigger::CmdlParsing(ERS_HERE, "Missing application name!"));
            std::cout << desc << std::endl;
            return EXIT_FAILURE;
        }
    }
    catch(std::exception& ex) {
        ers::error(daq::trigger::CmdlParsing(ERS_HERE, "", ex));
        return EXIT_FAILURE;
    }

    const char *partName = std::getenv("TDAQ_PARTITION");
    if(partName == nullptr) {
        std::cerr << "${TDAQ_PARTITION} is not set." << std::endl;
        return EXIT_FAILURE;
    }

    IPCPartition ipcPartition(partName);
    daq::trigger::MyMasterTrigger myTrigger;
    daq::trigger::CommandedTrigger *commandReceiver = new daq::trigger::CommandedTrigger(ipcPartition, name, &myTrigger);

    // sleep forever
    std::cin.ignore(std::numeric_limits < std::streamsize > ::max(), '\n');
    std::cin.get();
    ERS_LOG("Exiting application " << name);

    commandReceiver->_destroy(); //never delete this object!

    std::exit (EXIT_SUCCESS);
}
